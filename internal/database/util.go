package database

import (
	"fmt"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"strings"
)

const UrlName = "DB_URL"
const PasswordName = "DB_PASSWORD"
const UserName = "DB_USERNAME"
const LocalhostHost = "LOCALHOST"
const SslEnabled = "DB_SSL"
const SslDefaultEnabled = "true"
const DatabaseConnectionFilePath = "FILE_LOCATION"

func getConnectionParams(dbUrl, user, password string) (string, string, string, string, string, string, error) {
	parts := strings.Split(dbUrl, "://")
	leftPart := strings.Split(parts[1], "/")
	dbName := leftPart[1]
	hostPortPart := strings.Split(leftPart[0], ":")
	port := hostPortPart[1]
	host := hostPortPart[0]
	rightPart := strings.Split(parts[0], ":")
	dbType := rightPart[len(rightPart)-1]
	if strings.HasSuffix(strings.ToUpper(dbType), "POSTGRESQL") {
		dbType = "pgx"
	}
	return dbType, host, port, user, password, dbName, nil
}

func getConnectionParamsFromEnv() (string, string, string, string, string, string, error) {
	url := env.GetEnvVariable(UrlName, "jdbc:postgresql://localhost:5432/db")
	user := env.GetEnvVariable(UserName, "username")
	password := env.GetEnvVariable(PasswordName, "password")
	return getConnectionParams(url, user, password)
}

func GetDatabaseConnectionString() (string, string, error) {
	var dbType, host, port, user, password, dbName string
	var err error

	if len(env.GetEnvVariable(DatabaseConnectionFilePath, "")) == 0 {
		dbType, host, port, user, password, dbName, err = getConnectionParamsFromEnv()
	} else {
		dbType, host, port, user, password, dbName, err = getConnectionParamsFromFile()
	}

	if err != nil {
		return "", "", err
	}
	disabled := "require"
	if strings.ToUpper(host) == LocalhostHost || env.GetEnvVariable(SslEnabled, SslDefaultEnabled) != SslDefaultEnabled {
		disabled = "disable"
	}
	connectionInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		host, port, user, password, dbName, disabled)
	return dbType, connectionInfo, nil
}

func getConnectionParamsFromFile() (string, string, string, string, string, string, error) {
	pathFile := env.GetEnvVariable(DatabaseConnectionFilePath)
	items, err := util.LoadPropertiesFile(pathFile)
	if err != nil {
		return "", "", "", "", "", "", err
	}
	url := items[UrlName]
	user := items[UserName]
	password := items[PasswordName]
	return getConnectionParams(url, user, password)
}
