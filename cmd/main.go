package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"gitlab.com/mihael97/Go-utility/pkg/web/routes"
	"net/http"
)

type demoController struct {
	routes.RoutesController
}

func (demoController) GetRoutes() map[routes.Route]func(http.ResponseWriter, *http.Request) {
	return map[routes.Route]func(w http.ResponseWriter, r *http.Request){
		routes.CreateRoute("/", web.GET, false): func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("global"))
		},
		routes.CreateRoute("/{id}", web.GET, false): func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("id: " + r.PathValue("id")))
		},
	}
}
func (demoController) GetBasePath() string {
	return "/demo"
}

func main() {
	controllers := []routes.RoutesController{
		demoController{},
	}

	middleware1 := func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Info("First")
			handler.ServeHTTP(w, r)
		})
	}

	middleware2 := func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Info("Second")
			handler.ServeHTTP(w, r)
		})
	}

	engine := routes.CreateRouter(controllers, []func(handler http.Handler) http.Handler{middleware1, middleware2})

	log.Panic(http.ListenAndServe(":8080", engine))
}
