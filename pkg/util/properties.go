package util

import (
	"bufio"
	"os"
	"strings"
)

func LoadPropertiesFile(path string) (result map[string]string, err error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			return
		}
	}(file)
	scanner := bufio.NewScanner(file)
	result = make(map[string]string, 0)

	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "=")
		if len(parts) == 2 && (len(parts[0])+len(parts[1])) >= 2 {
			result[parts[0]] = parts[1]
		}
	}

	if scanner.Err() != nil {
		return nil, scanner.Err()
	}

	return
}
