package util

// Contains checks if value is inside collection
func Contains[T comparable](value T, values ...T) bool {
	for _, item := range values {
		if item == value {
			return true
		}
	}
	return false
}

// ContainsAll checks if all values from first list are part of second list
func ContainsAll[T comparable](first []T, second []T) bool {
	firstSet := Set(first...)
	secondSet := Set(second...)

	for _, item := range firstSet {
		if !Contains(item, secondSet...) {
			return false
		}
	}

	return true
}

// ContainsAny check if any value from first list is inside second list
func ContainsAny[T comparable](first []T, second []T) bool {
	for _, firstItem := range first {
		for _, secondValue := range second {
			if firstItem == secondValue {
				return true
			}
		}
	}
	return false
}

// Set unique values from collection
func Set[T comparable](values ...T) []T {
	hashMap := make(map[T]bool, 0)
	for _, value := range values {
		if _, contains := hashMap[value]; !contains {
			hashMap[value] = true
		}
	}

	return Keys(hashMap)
}

// Keys get all keys from map
func Keys[T comparable, V interface{} | []interface{}](items map[T]V) []T {
	keys := make([]T, len(items))

	index := 0
	for key := range items {
		keys[index] = key
		index++
	}

	return keys
}

// Map maps list of items to list of mapped items
func Map[A interface{}, B interface{}](items []A, mapFunction func(A) B) []B {
	results := make([]B, len(items))

	for index, item := range items {
		results[index] = mapFunction(item)
	}

	return results
}

func Filter[A interface{}](filterFunction func(item A) bool, values ...A) []A {
	filteredValues := make([]A, 0)

	for _, value := range values {
		if filterFunction(value) {
			filteredValues = append(filteredValues, value)
		}
	}

	return filteredValues
}

// SingletonList makes singleton list from one item
func SingletonList[T any](item T) []T {
	response := make([]T, 1)
	response[0] = item
	return response
}
