package util

import "time"

const DateLayout = "2006-08-31T15:04:05.123456Z"
const YYYYMMDDFormat = "2006-01-02"
const DDMMYYYYFormat = "02.01.2006."

func ParseDate(dateString string, additionalLayout ...string) (time.Time, error) {
	layout := DateLayout
	if len(additionalLayout) != 0 {
		layout = additionalLayout[0]
	}
	return time.Parse(layout, dateString)
}
