package web

type MethodType string

const (
	GET    MethodType = "GET"
	POST   MethodType = "POST"
	DELETE MethodType = "DELETE"
	PUT    MethodType = "PUT"
	ALL    MethodType = "ALL"
)

func (m MethodType) String() string {
	return string(m)
}
