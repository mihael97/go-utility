package web

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"net/http"
)

func ParseToJson(data interface{}, resp http.ResponseWriter, responseStatus int) {
	writeWithContentType(data, "application/json", resp, responseStatus)
}

func writeWithContentType(data interface{}, contentType string, w http.ResponseWriter, responseStatus int) {
	if data == nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(responseStatus)
	_ = json.NewEncoder(w).Encode(data)
}

func WriteError(err error, resp http.ResponseWriter) {
	WriteErrorMessage(err.Error(), resp)
}

func WriteErrorMessage(err string, resp http.ResponseWriter) {
	ParseToJson(util.NewException(err), resp, http.StatusBadRequest)
}

func ParseParams(paramNames []string, req *http.Request) (map[string]string, error) {
	var params map[string]string
	err := json.NewDecoder(req.Body).Decode(&params)
	if err != nil {
		return params, err
	}

	caser := cases.Title(language.AmericanEnglish)

	for _, name := range paramNames {
		param, ok := params[name]
		if !ok {
			return params, errors.New(fmt.Sprintf("%s should be provided", caser.String(name)))
		}
		params[name] = param
	}
	return params, nil
}

func parseError(err error) map[string][]string {
	if validationErrs, ok := err.(validator.ValidationErrors); ok {
		errorMessages := make(map[string][]string, len(validationErrs))
		for _, err := range validationErrs {
			field := err.Field()
			value, exist := errorMessages[field]
			if !exist {
				value = make([]string, 0)
			}
			value = append(value, err.Tag())
			errorMessages[field] = value
		}
		return errorMessages
	}
	return nil
}
