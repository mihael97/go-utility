package routes

import (
	"fmt"
	"gitlab.com/mihael97/Go-utility/pkg/web"
)

type Route struct {
	URL     string
	Type    web.MethodType
	Secured bool
	Roles   *[]string
}

func (method *Route) getType() string {
	return fmt.Sprint(method.Type)
}

func CreateRoute(path string, method web.MethodType, secured bool) Route {
	return Route{
		URL:     path,
		Type:    method,
		Secured: secured,
	}
}
