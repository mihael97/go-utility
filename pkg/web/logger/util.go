package logger

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/negroni"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"math"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Config struct {
	PrettyPrint bool
	ReleaseMode bool
}

func loggingMiddleware(logger *log.Logger, notLogged ...string) func(http.Handler) http.Handler {
	return func(nextHandler http.Handler) http.Handler {
		hostname, err := os.Hostname()
		if err != nil {
			hostname = "unknow"
		}

		var skip map[string]struct{}

		if length := len(notLogged); length > 0 {
			skip = make(map[string]struct{}, length)

			for _, p := range notLogged {
				skip[p] = struct{}{}
			}
		}

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// other handler can change c.Path so:
			path := r.URL.Path
			start := time.Now()
			newWriter := negroni.NewResponseWriter(w)
			nextHandler.ServeHTTP(newWriter, r)
			stop := time.Since(start)
			latency := int(math.Ceil(float64(stop.Nanoseconds()) / 1000000.0))
			statusCode := newWriter.Status()
			clientIP := r.Header.Get("X-Forwarded-For ")
			clientUserAgent := r.UserAgent()
			referer := r.Referer()

			dataLength, _ := strconv.Atoi(w.Header().Get("ContentLength"))

			if dataLength < 0 {
				dataLength = 0
			}

			if _, ok := skip[path]; ok {
				return
			}

			entry := logger.WithFields(log.Fields{
				"hostname":   hostname,
				"statusCode": statusCode,
				"latency":    latency, // time to process
				"clientIP":   clientIP,
				"method":     r.Method,
				"path":       path,
				"referer":    referer,
				"dataLength": dataLength,
				"userAgent":  clientUserAgent,
			})

			msg := fmt.Sprintf("%s - %s [%s] \"%s %s\" %d %d \"%s\" \"%s\" (%dms)", clientIP, hostname, time.Now().Format(time.DateTime), r.Method, path, statusCode, dataLength, referer, clientUserAgent, latency)
			if statusCode >= http.StatusInternalServerError {
				entry.Error(msg)
			} else if statusCode >= http.StatusBadRequest {
				entry.Warn(msg)
			} else {
				entry.Info(msg)
			}
		})
	}
}

func InitLogger(config Config) func(http.Handler) http.Handler {
	if env.GetEnvVariable("PROFILE", "local") == "prod" {
		jsonFormatter := log.JSONFormatter{PrettyPrint: config.PrettyPrint}
		log.SetFormatter(&jsonFormatter)
		log.SetOutput(os.Stdout)
		loggerInstance := InitCustomLoggerHook(CustomLoggerHookConfig{
			RollingFile: true,
		})
		log.AddHook(loggerInstance)

		logger := log.New()
		logger.SetFormatter(&jsonFormatter)
		logger.AddHook(loggerInstance)
		return loggingMiddleware(logger)
	}
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handler.ServeHTTP(w, r)
		})
	}
}

func InitDefaultLogger() func(http.Handler) http.Handler {
	if env.GetEnvVariable("PROFILE", "local") == "prod" {
		prettyPrint, _ := strconv.ParseBool(env.GetEnvVariable("PRETTY_PRINT", "false"))
		return InitLogger(Config{PrettyPrint: prettyPrint, ReleaseMode: true})
	}
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handler.ServeHTTP(w, r)
		})
	}
}
