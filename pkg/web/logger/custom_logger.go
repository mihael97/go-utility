package logger

import (
	"archive/zip"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"gitlab.com/mihael97/Go-utility/pkg/web/security/context"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type CustomLoggerHookConfig struct {
	RollingFile             bool
	RollingFileNextFile     *func(fileName string) bool
	RollingFileNextFileName *func() string
}

type CustomLoggerHook struct {
	fileName string
	config   CustomLoggerHookConfig
}

func InitCustomLoggerHook(config CustomLoggerHookConfig) log.Hook {
	checkConfig(&config)
	fileName := (*config.RollingFileNextFileName)()
	return CustomLoggerHook{fileName: fileName, config: config}
}

func checkConfig(c *CustomLoggerHookConfig) {
	if c.RollingFile {
		if c.RollingFileNextFile == nil {
			c.RollingFileNextFile = util.GetPointer(func(fileName string) bool {
				year, month, day := time.Now().Date()
				fileTimeParts := strings.Split(strings.Split(fileName, "_")[1], "-")
				result :=
					strconv.FormatInt(int64(year), 10) == fileTimeParts[0] && month.String() == fileTimeParts[1] && strconv.FormatInt(int64(day), 10) == fileTimeParts[2]
				if result {
					go archiveFile(fileName)
				}
				return result
			})
		}

		if c.RollingFileNextFileName == nil {
			c.RollingFileNextFileName = util.GetPointer(func() string {
				year, month, day := time.Now().Date()
				logsDirectory := env.GetEnvVariable("LOGS_DIRECTORY", "./")
				appName := env.GetEnvVariable("APP_NAME", "")
				path := filepath.Join(logsDirectory, appName, fmt.Sprintf("files_%d-%s-%d.log", year, month.String(), day))
				return path
			})
		}
	}
}

func archiveFile(name string) {
	defer func(name string) {
		_ = os.Remove(name)
	}(name)

	dir := filepath.Dir(name)
	fileName := filepath.Base(name)
	newFileName := strings.ReplaceAll(fileName, ".log", ".zip")

	archive, err := os.Create(filepath.Join(dir, newFileName))
	if err != nil {
		log.Panicln(err)
	}
	defer func(archive *os.File) {
		_ = archive.Close()
	}(archive)

	zipWriter := zip.NewWriter(archive)

	f1, err := os.Open(name)
	if err != nil {
		log.Panicln(err)
	}
	defer func(f1 *os.File) {
		_ = f1.Close()
	}(f1)

	w1, err := zipWriter.Create(fileName)
	if err != nil {
		log.Panicln(err)
	}
	if _, err := io.Copy(w1, f1); err != nil {
		log.Panicln(err)
	}
	_ = zipWriter.Close()
}

func (c CustomLoggerHook) Levels() []log.Level {
	return []log.Level{log.DebugLevel, log.InfoLevel, log.ErrorLevel, log.PanicLevel, log.FatalLevel}
}

func (c CustomLoggerHook) Fire(entry *log.Entry) error {
	if context.HasUserContext() {
		userContext := context.GetUserContext()
		if len(userContext.Id) != 0 {
			entry.Data["user-id"] = userContext.Id
		}
	}

	c.changeLogger(entry)

	return nil
}

func (c *CustomLoggerHook) changeLogger(entry *log.Entry) {
	if c.config.RollingFile {
		if (*c.config.RollingFileNextFile)(c.fileName) {
			newFileName := (*c.config.RollingFileNextFileName)()
			c.fileName = newFileName
		}
		createdFile := openFile(c.fileName)
		entry.Logger.Out = createdFile
	}
}

func openFile(name string) *os.File {
	file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		return file
	} else {
		log.Info("Failed to log to file, using default stderr")
	}
	return nil
}
