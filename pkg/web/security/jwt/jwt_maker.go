package jwt

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/web/security"
	"net/http"
	"regexp"
	"time"
)

type jwtMaker struct {
	secretKey string
}

func (maker *jwtMaker) Login(username string, resp http.ResponseWriter, _ *http.Request) (string, error) {
	payload := security.Payload{
		ID:        uuid.NewString(),
		Username:  username,
		IssuedAt:  time.Time{},
		ExpiredAt: time.Time{},
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, &payload)
	token, err := jwtToken.SignedString([]byte(maker.secretKey))
	if err != nil {
		return "", err
	}
	resp.Header().Set(env.GetEnvVariable(AuthorizationHeaderEnvKey, DefaultAuthorizationHeaderName), token)
	return token, nil
}

func (maker *jwtMaker) VerifyToken(r *http.Request) (*security.Payload, error) {
	header := r.Header.Get(env.GetEnvVariable(AuthorizationHeaderEnvKey, DefaultAuthorizationHeaderName))
	if len(header) == 0 {
		return nil, errors.New("header not provided")
	}
	headerArr := regexp.MustCompile(BearerTokenRegex).Split(header, -1)
	if len(headerArr) != 2 {
		return nil, errors.New("wrong header format")
	}
	token := headerArr[1]
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid token")
		}
		return []byte(maker.secretKey), nil
	}
	jwtToken, err := jwt.ParseWithClaims(token, &security.Payload{}, keyFunc)
	if err != nil {
		return nil, err
	}
	payload, ok := jwtToken.Claims.(*security.Payload)
	if !ok {
		return nil, errors.New("invalid token")
	}
	return payload, nil
}

func NewJwtMaker(secretKey string) (security.AuthProvider, error) {
	if len(secretKey) < MinSecretKeySize {
		return nil, fmt.Errorf("invalid key size: must be at least %v", MinSecretKeySize)
	}
	return &jwtMaker{secretKey: secretKey}, nil
}
