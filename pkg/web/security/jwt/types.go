package jwt

const AuthorizationHeaderEnvKey = "AUTH_HEADER"
const DefaultAuthorizationHeaderName = "Authorization"
const MinSecretKeySize = 32
const BearerTokenRegex = "\\s*(\\s|:)\\s*"
const SecretVariable = "JWT_SECRET"
