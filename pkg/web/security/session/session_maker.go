package session

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/hashicorp/go-uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/web/security"
	"golang.org/x/net/context"
	"net/http"
	"strings"
	"time"
)

type sessionMakerImpl struct {
	client   *redis.Client
	validity time.Duration
}

type sessionCookiePayload struct {
	ExpirationDate int64
	IssueDate      int64
	Username       string
}

func (i sessionCookiePayload) MarshalBinary() ([]byte, error) {
	return json.Marshal(i)
}

func (s sessionMakerImpl) Login(username string, resp http.ResponseWriter, r *http.Request) (string, error) {
	token, err := uuid.GenerateUUID()

	if err != nil {
		return "", err
	}

	currentTime := time.Now().UnixMilli()
	cookiePayload := sessionCookiePayload{
		IssueDate:      currentTime,
		ExpirationDate: currentTime + s.validity.Milliseconds(),
		Username:       username,
	}

	err = s.client.Set(context.Background(), token, cookiePayload, s.validity).Err()
	if err != nil {
		return "", err
	}

	host := r.Host
	if strings.HasPrefix(host, "localhost") {
		logrus.Info("Setting localhost")
		host = "localhost"
	}

	isHttps := host != "localhost"

	cookie := http.Cookie{
		Name:     GetSessionTokenName(),
		Value:    token,
		Path:     "/",
		Domain:   host,
		MaxAge:   4 * 60 * 60,
		Secure:   isHttps,
		HttpOnly: isHttps,
	}

	if isHttps {
		cookie.SameSite = http.SameSiteNoneMode
	}

	http.SetCookie(resp, &cookie)

	return token, nil
}

func (s sessionMakerImpl) VerifyToken(r *http.Request) (*security.Payload, error) {
	sessionCookieItem, err := r.Cookie(GetSessionTokenName())
	if err != nil {
		return nil, err
	}
	sessionCookie := sessionCookieItem.Value
	if len(sessionCookie) == 0 {
		return nil, fmt.Errorf("token not provided")
	}

	cookie := s.client.Get(context.Background(), sessionCookie)
	if cookie.Err() != nil {
		if errors.Is(cookie.Err(), redis.Nil) {
			return nil, fmt.Errorf("session not found")
		}
		return nil, err
	}

	var cookiePayload sessionCookiePayload
	err = json.Unmarshal([]byte(cookie.Val()), &cookiePayload)
	if err != nil {
		return nil, err
	}

	return &security.Payload{
		ID:        sessionCookie,
		Username:  cookiePayload.Username,
		IssuedAt:  time.UnixMilli(cookiePayload.IssueDate),
		ExpiredAt: time.UnixMilli(cookiePayload.ExpirationDate),
	}, nil
}

func GetSessionMaker() security.AuthProvider {
	return &sessionMakerImpl{
		redis.NewClient(&redis.Options{
			Addr:     env.GetEnvVariable("REDIS_URL"),
			Password: env.GetEnvVariable("REDIS_PASSWORD", ""),
			DB:       0,
		}),
		time.Hour,
	}
}
