package security

import (
	"errors"
	"net/http"
	"time"
)

type Token struct {
	Token string `json:"token"`
}

type Payload struct {
	ID        string    `json:"id"`
	Username  string    `json:"username"`
	IssuedAt  time.Time `json:"issued_at"`
	ExpiredAt time.Time `json:"expired_at"`
}

func (p *Payload) Valid() error {
	if time.Now().After(p.ExpiredAt) {
		return errors.New("token has expired")
	}
	return nil
}

type AuthProvider interface {
	Login(string, http.ResponseWriter, *http.Request) (string, error)
	VerifyToken(*http.Request) (*Payload, error)
}
