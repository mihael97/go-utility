package context

type UserContext struct {
	Id       string
	Username string
	Roles    []string
	Token    string
}
