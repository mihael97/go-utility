package context

import (
	"gitlab.com/mihael97/Go-utility/pkg/web/security/jwt"
	"net/http"
	"strings"

	"github.com/timandy/routine"
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"gitlab.com/mihael97/Go-utility/pkg/util"
)

const IdHeaderName = "ID_HEADER"
const IdHeaderDefaultValue = "X-MACUKA-ID"
const UsernameHeaderName = "USERNAME_HEADER"
const UsernameHeaderDefaultValue = "X-MACUKA-USERNAME"
const RolesHeaderName = "ROLES_HEADER"
const RolesHeaderDefaultValue = "X-MACUKA-ROLES"

var localContext routine.ThreadLocal[*UserContext]

func StartUserContext(r *http.Request) {
	localContext = routine.NewThreadLocal[*UserContext]()

	idHeader := env.GetEnvVariable(IdHeaderName, IdHeaderDefaultValue)
	id := r.Header.Get(idHeader)

	usernameHeader := env.GetEnvVariable(UsernameHeaderName, UsernameHeaderDefaultValue)
	username := r.Header.Get(usernameHeader)

	rolesHeader := env.GetEnvVariable(RolesHeaderName, RolesHeaderDefaultValue)
	rolesValue := r.Header.Get(rolesHeader)
	roles := util.Map(strings.Split(rolesValue, ","), func(value string) string {
		return strings.TrimSpace(value)
	})

	authorizationHeaderName := env.GetEnvVariable(jwt.AuthorizationHeaderEnvKey, jwt.DefaultAuthorizationHeaderName)

	context := UserContext{
		Id:       id,
		Username: username,
		Roles:    roles,
		Token:    r.Header.Get(authorizationHeaderName),
	}
	localContext.Set(&context)
}

func SetUserContext(context UserContext) {
	if localContext == nil {
		localContext = routine.NewThreadLocal[*UserContext]()
	}
	localContext.Set(&context)
}

func GetUserContext() UserContext {
	if localContext == nil || localContext.Get() == nil {
		return UserContext{}
	}
	context := localContext.Get()
	return *context
}

func HasUserContext() bool {
	return localContext != nil && localContext.Get() != nil
}
