package currency

import (
	"encoding/json"
	"gitlab.com/mihael97/Go-utility/pkg/currency"
	"io/ioutil"
	"log"
	"testing"
)

func TestCurrencyParsing(t *testing.T) {
	content, err := ioutil.ReadFile("./data.json")
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}
	var jsonContent []map[string]interface{}
	if err := json.Unmarshal(content, &jsonContent); err != nil {
		log.Fatal(err)
	}

	actualCurrencies, err := currency.ParseCurrencies(jsonContent)
	if err != nil {
		log.Fatal(err)
	}

	if len(actualCurrencies) != 13 {
		t.Errorf("size wrong. Should be 13 but is %d", len(actualCurrencies))
	}
	firstCurrency := actualCurrencies[0]
	if firstCurrency.Name != "AUD" {
		t.Errorf("name should be AUD but is %s", firstCurrency.Name)
	}
	if firstCurrency.MiddleRate != 1.5529 {
		t.Errorf("middle rate should be 1.5529 but is %f", firstCurrency.MiddleRate)
	}
	if firstCurrency.SellingRate != 1.5506 {
		t.Errorf("middle rate should be 1.5506 but is %f", firstCurrency.SellingRate)
	}
	if firstCurrency.BuyingRate != 1.5552 {
		t.Errorf("middle rate should be 1.5552 but is %f", firstCurrency.BuyingRate)
	}
}
