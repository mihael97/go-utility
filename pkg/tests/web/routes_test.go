package web

import (
	"fmt"
	"gitlab.com/mihael97/Go-utility/pkg/web"
	"gitlab.com/mihael97/Go-utility/pkg/web/routes"
	"testing"
)

const DefaultUrl = "localhost:8080"
const DefaultMethod = web.GET
const DefaultSecured = true

func TestRoutes(t *testing.T) {
	route := routes.CreateRoute(DefaultUrl, DefaultMethod, DefaultSecured)

	if route.URL != DefaultUrl {
		t.Error(fmt.Sprintf("Url is %s but should be %s", route.URL, DefaultUrl))
	}

	if route.Type != DefaultMethod {
		t.Error(fmt.Sprintf("Type is %s but should be %s", route.Type, DefaultMethod))
	}

	if route.Secured != DefaultSecured {
		t.Error(fmt.Sprintf("Secured is %v but should be %v", route.Secured, DefaultSecured))
	}
}
