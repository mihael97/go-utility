package util

import (
	"bytes"
	"encoding/json"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"testing"
)

func TestLoadPropertiesFile(t *testing.T) {
	items, err := util.LoadPropertiesFile("./testFile.properties")

	if err != nil {
		t.Errorf("Error is not nil. %v", err)
	} else {
		if len(items) != 2 {
			t.Errorf("Size is not 2. It is %d", len(items))
		} else {
			expectedResult := map[string]string{
				"a":    "b",
				"hehe": "lol",
			}
			expectedResultJson, _ := json.Marshal(expectedResult)

			actualResultJson, _ := json.Marshal(items)

			if bytes.Compare(expectedResultJson, actualResultJson) != 0 {
				t.Errorf("Not the same.\nExpected: %s\nActual: %s", string(expectedResultJson), string(actualResultJson))
			}
		}
	}
}
