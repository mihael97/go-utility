package util

import (
	"testing"

	"gitlab.com/mihael97/Go-utility/pkg/util"
)

func TestString(t *testing.T) {
	expectedValue := "s"

	actualValue := util.GetPointer(expectedValue)

	if actualValue == nil {
		t.Error("Value is null")
	} else if *actualValue != expectedValue {
		t.Errorf("Value is %v but should be %v", *actualValue, expectedValue)
	}
}

func TestTrue(t *testing.T) {
	expectedValue := true

	actualValue := util.GetPointer(expectedValue)

	if actualValue == nil {
		t.Error("Value is null")
	} else if *actualValue != expectedValue {
		t.Errorf("Value is %v but should be %v", *actualValue, expectedValue)
	}
}
