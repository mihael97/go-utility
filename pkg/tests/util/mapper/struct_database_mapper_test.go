package mapper

import (
	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/mihael97/Go-utility/pkg/mapper"
	util "gitlab.com/mihael97/Go-utility/pkg/types"
	util2 "gitlab.com/mihael97/Go-utility/pkg/util"
	"log"
	"testing"
	"time"
)

type testStructStruct struct {
	Id         string `dbName:"id"`
	Name       string
	IntValue   int `dbName:"hehe"`
	FloatValue *float64
	Date       time.Time `dbName:"date"`
	DateMillis util.MillisTime
}

func TestStructNull(t *testing.T) {
	db, mock, _ := sqlmock.New()
	rows := sqlmock.NewRows([]string{"float_value"}).AddRow(1.2)
	mock.ExpectBegin()
	mock.ExpectQuery("SELECT").WillReturnRows(rows)
	mock.ExpectCommit()

	tx, _ := db.Begin()
	queryRows, err := tx.Query("SELECT")
	if err != nil {
		panic(err)
	}

	databaseMapper := mapper.GetStructDatabaseMapper[testStructStruct]()

	expectedResults := []testStructStruct{
		{FloatValue: util2.GetPointer(1.2)},
	}

	actualResults, err := databaseMapper.MapItems(queryRows)
	if err != nil {
		t.Error(err)
	} else {
		if actualResults == nil {
			t.Errorf("Item is null")
		} else {
			for i := range expectedResults {
				log.Printf("Row %d\n", i)
				actualResult := actualResults[i]
				expectedResult := expectedResults[i]
				if *actualResult.FloatValue != *expectedResult.FloatValue {
					t.Errorf("Float_value should be %v but is %v", *expectedResult.FloatValue, *actualResult.FloatValue)
				}
			}
		}
	}
}

func TestStructDate(t *testing.T) {
	db, mock, _ := sqlmock.New()
	rows := sqlmock.NewRows([]string{"date", "date_millis"}).AddRow(time.Date(2023, 1, 1, 1, 1, 1, 1, time.Local), time.Date(2023, 1, 1, 1, 1, 1, 1, time.Local))
	mock.ExpectBegin()
	mock.ExpectQuery("SELECT").WillReturnRows(rows)
	mock.ExpectCommit()

	tx, _ := db.Begin()
	queryRows, err := tx.Query("SELECT")
	if err != nil {
		panic(err)
	}

	databaseMapper := mapper.GetStructDatabaseMapper[testStructStruct]()

	expectedResults := []testStructStruct{
		{Date: time.Date(2023, 1, 1, 1, 1, 1, 1, time.Local), DateMillis: *util.FromDate(time.Date(2023, 1, 1, 1, 1, 1, 1, time.Local))},
	}

	actualResults, err := databaseMapper.MapItems(queryRows)
	if err != nil {
		t.Error(err)
	} else {
		if actualResults == nil {
			t.Errorf("Item is null")
		} else {
			for i := range expectedResults {
				log.Printf("Row %d\n", i)
				actualResult := actualResults[i]
				expectedResult := expectedResults[i]
				if actualResult.Date != expectedResult.Date {
					t.Errorf("Date should be %v but is %v", expectedResult.Date, actualResult.Date)
				}
				if actualResult.DateMillis.ToTime() != expectedResult.DateMillis.ToTime() {
					t.Errorf("Date_millis should be %v but is %v", expectedResult.DateMillis, actualResult.DateMillis)
				}
			}
		}
	}
}

func TestStructDifferentName(t *testing.T) {
	db, mock, _ := sqlmock.New()
	rows := sqlmock.NewRows([]string{"hehe"}).AddRow(1)
	mock.ExpectBegin()
	mock.ExpectQuery("SELECT").WillReturnRows(rows)
	mock.ExpectCommit()

	tx, _ := db.Begin()
	queryRows, err := tx.Query("SELECT")
	if err != nil {
		panic(err)
	}

	databaseMapper := mapper.GetStructDatabaseMapper[testStructStruct]()

	expectedResults := []testStructStruct{
		{IntValue: 1},
	}

	actualResults, err := databaseMapper.MapItems(queryRows)
	if err != nil {
		t.Error(err)
	} else {
		if actualResults == nil {
			t.Errorf("Item is null")
		} else {
			for i := range expectedResults {
				log.Printf("Row %d\n", i)
				actualResult := actualResults[i]
				expectedResult := expectedResults[i]
				if actualResult.IntValue != expectedResult.IntValue {
					t.Errorf("Date should be %v but is %v", expectedResult.IntValue, actualResult.IntValue)
				}
			}
		}
	}
}
