package env

import (
	"gitlab.com/mihael97/Go-utility/pkg/env"
	"testing"
)

type demoStruct struct {
	Name    string `env:"name"`
	Default string `env:"name2,default:defaultValue"`
	Empty   string `env:"name3,omniempty"`
}

func TestEnvStruct(t *testing.T) {

	testCases := map[string]struct {
		CheckResult func(value demoStruct, t *testing.T)
	}{
		"standard": {
			CheckResult: func(value demoStruct, t *testing.T) {
				if value.Name != "hehe" {
					t.Errorf("Expected 'hehe', got %s", value.Name)
				}
			},
		},
		"default": {
			CheckResult: func(value demoStruct, t *testing.T) {
				if value.Default != "defaultValue" {
					t.Errorf("Expected 'defaultValue', got %s", value.Default)
				}
			},
		},
		"empty": {
			CheckResult: func(value demoStruct, t *testing.T) {
				if len(value.Empty) != 0 {
					t.Errorf("Expected empty, got %s", value.Empty)
				}
			},
		},
	}

	t.Setenv("name", "hehe")

	val := demoStruct{}
	env.PopulateEnvVariables(&val)

	for testCase, content := range testCases {
		t.Run(testCase, func(t *testing.T) {
			content.CheckResult(val, t)
		})
	}
}
