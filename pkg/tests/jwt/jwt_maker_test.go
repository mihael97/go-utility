package jwt

import (
	"gitlab.com/mihael97/Go-utility/pkg/web/security/jwt"
	"regexp"
	"testing"
)

func TestBearerTokenWithSpaces(t *testing.T) {
	value := "bearer                      a"
	actualValue := regexp.MustCompile(jwt.BearerTokenRegex).Split(value, -1)
	if len(actualValue) != 2 {
		t.Error("Wrong number of parts")
	}
	if actualValue[0] != "bearer" {
		t.Errorf("First part should be 'bearer' but is %v\n", actualValue[0])
	}
	if actualValue[1] != "a" {
		t.Errorf("Second part should be 'bearer' but is %v\n", actualValue[1])
	}
}

func TestBearerTokenWithDoubleDotsAndSpaces(t *testing.T) {
	value := "bearer              :        a"
	actualValue := regexp.MustCompile(jwt.BearerTokenRegex).Split(value, -1)
	if len(actualValue) != 2 {
		t.Error("Wrong number of parts")
	}
	if actualValue[0] != "bearer" {
		t.Errorf("First part should be 'bearer' but is %v\n", actualValue[0])
	}
	if actualValue[1] != "a" {
		t.Errorf("Second part should be 'bearer' but is %v\n", actualValue[1])
	}
}

func TestBearerTokenWithDoubleDots(t *testing.T) {
	value := "bearer:a"
	actualValue := regexp.MustCompile(jwt.BearerTokenRegex).Split(value, -1)
	if len(actualValue) != 2 {
		t.Error("Wrong number of parts")
	}
	if actualValue[0] != "bearer" {
		t.Errorf("First part should be 'bearer' but is %v\n", actualValue[0])
	}
	if actualValue[1] != "a" {
		t.Errorf("Second part should be 'bearer' but is %v\n", actualValue[1])
	}
}
