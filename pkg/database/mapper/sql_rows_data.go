package mapper

import (
	"strconv"
)

type sqlRowsDataImpl struct {
	index int
	data  []map[string]*interface{}
}

func (s *sqlRowsDataImpl) Size() int {
	return len(s.data)
}

func (s *sqlRowsDataImpl) IsEmpty() bool {
	return len(s.data) == 0
}

func (s *sqlRowsDataImpl) HasValue(field string) bool {
	return s.data[s.index][field] != nil
}

func (s *sqlRowsDataImpl) Contains(field string) bool {
	_, exists := s.data[s.index][field]
	return exists
}

func extractData[T any](data map[string]*interface{}, field string, mapFunction func(item *interface{}) T, defaultValue T) T {
	if data[field] == nil {
		return defaultValue
	}
	return mapFunction(data[field])
}

func (s *sqlRowsDataImpl) GetFloat64(field string) float64 {
	return extractData(s.data[s.index], field, func(item *interface{}) float64 {
		if item == nil {
			return -1
		}

		switch (*item).(type) {
		case string:
			value := (*item).(string)
			if len(value) == 0 {
				return -1
			}
			floatValue, _ := strconv.ParseFloat(value, 64)
			return floatValue
		case float64:
			return (*item).(float64)
		default:
			return -1
		}

	}, 0)
}

func (s *sqlRowsDataImpl) GetBool(field string) bool {
	return extractData(s.data[s.index], field, func(item *interface{}) bool {
		return (*item).(bool)
	}, false)
}

func (s *sqlRowsDataImpl) GetInt64(field string) int64 {
	return extractData(s.data[s.index], field, func(item *interface{}) int64 {
		return (*item).(int64)
	}, 0)
}

func (s *sqlRowsDataImpl) GetInt(field string) int {
	return extractData(s.data[s.index], field, func(item *interface{}) int {
		return int((*item).(int64))
	}, 0)
}

func (s *sqlRowsDataImpl) GetData(field string) *interface{} {
	if s.data[s.index][field] != nil {
		return s.data[s.index][field]
	}
	return nil
}

func (s *sqlRowsDataImpl) GetString(field string) string {
	return extractData(s.data[s.index], field, func(item *interface{}) string {
		return (*item).(string)
	}, "")
}

func (s *sqlRowsDataImpl) HasNext() bool {
	s.index += 1
	return s.index < len(s.data)
}

func (s *sqlRowsDataImpl) RowAtIndex(index int) map[string]*interface{} {
	if index >= len(s.data) {
		return nil
	}
	return s.data[index]
}

func CreateSqlRowsData(data []map[string]*interface{}) SqlRowsData {
	if data == nil {
		return &sqlRowsDataImpl{data: []map[string]*interface{}{}}
	}
	return &sqlRowsDataImpl{
		index: -1,
		data:  data,
	}
}
