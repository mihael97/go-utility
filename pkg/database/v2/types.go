package dao

import "database/sql"

type DB[T comparable] interface {
	Create(query string, args ...any) (*int64, error)
	CreateNamed(string, T) (*int64, error)
	QueryForList(string, func(row *sql.Rows) (*T, error), ...any) ([]T, error)
	StartTransaction() error
	CommitTransaction() error
	StopTransaction() error
	GetTransaction() (*sql.Tx, error)
	GetRows(string, ...any) (*sql.Rows, error)
	Execute(string, ...any) error
	ExecuteNamed(string, T) error
	QueryForObject(string, func(row *sql.Row) (*T, error), ...any) (*T, error)
}
