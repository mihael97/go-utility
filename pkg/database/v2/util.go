package dao

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"sort"
	"strings"
)

func openConnection() *sql.DB {
	username := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	parts := strings.Split(os.Getenv("DB_URL"), "://")
	secondParts := strings.Split(parts[1], "/")
	dbName := secondParts[1]
	dbHost := strings.Split(secondParts[0], ":")
	item, err := sql.Open("postgres", fmt.Sprintf("user=%s dbname=%s sslmode=disable password=%s host=%s port=%s", username, dbName, dbPassword, dbHost[0], dbHost[1]))
	if err != nil {
		logrus.Panic(err)
	}
	return item
}

func getArgs[T comparable](item T) (args []any, err error) {
	jsonBody, err := json.Marshal(item)
	if err != nil {
		return
	}
	argsMap := make(map[string]interface{})
	err = json.Unmarshal(jsonBody, &argsMap)
	if err != nil {
		return
	}

	keys := make([]string, 0)
	for key := range argsMap {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	args = make([]any, 0)
	for _, key := range keys {
		args = append(args, sql.Named(key, argsMap[key]))
	}

	return
}

func ExtractRows[T comparable](rows *sql.Rows, mappingFunction func(rows2 *sql.Rows) (*T, error)) ([]T, error) {
	if rows == nil {
		return nil, fmt.Errorf("rows are nil")
	}
	results := make([]T, 0)

	for rows.Next() {
		item, err := mappingFunction(rows)
		if err != nil {
			return nil, err
		}

		if item == nil {
			continue
		}

		results = append(results, *item)
	}

	return results, nil
}
