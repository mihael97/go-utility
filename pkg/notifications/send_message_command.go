package dto

type Channel string

const EmailChannel Channel = "EMAIL"
const PushChannel Channel = "PUSH"

type SendMessageCommand struct {
	SendMessageCommon
	MessageId string            `json:"messageId"`
	Client    string            `json:"client"`
	Data      map[string]string `json:"data"`
	Channel   string            `json:"channel"`
}
