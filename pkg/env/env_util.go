package env

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"os"
	"reflect"
	"strings"
)

func GetEnvVariable(name string, defaultValue ...string) string {
	value := os.Getenv(name)
	if len(value) == 0 {
		if len(defaultValue) != 0 {
			return defaultValue[0]
		}
		log.Fatalf("Environemental variable %s not present", name)
	}
	return value
}

func PopulateEnvVariables(data interface{}) {
	if data == nil {
		return
	}

	value := reflect.ValueOf(data)
	for i := 0; i < value.Elem().NumField(); i++ {
		envTag := reflect.TypeOf(data).Elem().Field(i).Tag.Get("env")
		if len(envTag) == 0 {
			continue
		}
		canSet := reflect.ValueOf(data).Elem().Field(0).CanSet()
		if canSet {
			setValue(value, i, envTag)
		}
	}
}

func setValue(value reflect.Value, index int, tag string) {
	var valueToSet *string
	parts := strings.Split(tag, ",")

	if len(parts) == 1 {
		valueToSet = util.GetPointer(os.Getenv(parts[0]))
	} else {
		mapValues := make(map[string]string, len(parts[1:]))
		for _, part := range parts[1:] {
			tagParts := strings.Split(part, ":")
			var tagValue string

			if len(tagParts) == 1 {
				tagValue = ""
			} else {
				tagValue = strings.TrimSpace(tagParts[1])
			}

			mapValues[strings.TrimSpace(tagParts[0])] = tagValue
		}

		for tagName, tagValue := range mapValues {
			switch strings.ToUpper(tagName) {
			case "DEFAULT":
				valueToSet = &tagValue
			case "OMNIEMPTY":
				return
			}
		}
	}

	if valueToSet == nil {
		log.Panicln("Value is null")
	}

	value.Elem().Field(index).SetString(*valueToSet)
}
