package util

import (
	"gitlab.com/mihael97/Go-utility/pkg/util"
	"log"
	"strings"
	"time"
)

type CustomTime time.Time

func (c *CustomTime) UnmarshalJSON(content []byte) error {
	date, err := util.ParseDate(strings.ReplaceAll(string(content), "\"", ""), util.YYYYMMDDFormat)
	if err != nil {
		log.Println(err)
		return nil
	}
	*c = CustomTime(date)
	return nil
}
