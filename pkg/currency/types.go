package currency

type CurrencyService interface {
	GetRates() ([]Currency, error)
	Convert(fromRate float64, toRate float64, amount float64) float64
	GetByName(name string) (*Currency, error)
}
