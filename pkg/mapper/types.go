package mapper

// Mapper Generic Mapper interface
type Mapper[T any, M any] interface {
	MapItem(row T) *M
	MapItems(rows []T) []M
}
