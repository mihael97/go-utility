package swagger

import (
	"github.com/swaggo/swag"
	"net/http"
)

type SwaggerConfig struct {
	ApiPath string
	AppName *string
	Docs    *swag.Spec
	Router  *http.ServeMux
}
